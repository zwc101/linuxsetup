echo "Initializing System Rebuild"

#We need Curl for some steps
sudo apt install curl

#Install Rust
curl https://sh.rustup.rs -sSf | sh

#Install Code

#Install vim as well
sudo apt install vim

#Install Firefox-dev
sudo apt install ubuntu-make
umake web firefox-dev
#TODO: Configure proper path to dev

#Install Docker
sudo apt install docker.io

#Install slack
sudo apt install slack

#Install Kubuntu
sudo add-apt-repository ppa:kubuntu-ppa/backports
sudo apt update && sudo apt install kubuntu-desktop

#Install Kubuntu settings

#Configure bitbucket keys

#Configure git with user info
git config --global user.email "zach.childers@berkeley.edu"
git config --global user.name "Zach Childers"